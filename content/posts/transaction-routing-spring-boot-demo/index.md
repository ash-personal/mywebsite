+++ 
draft = false
date = 2023-09-11T11:23:36+05:30
title = "Postgres primary and read replica demonstration using a spring boot project"
description = ""
slug = ""
authors = []
tags = ["database-read-replica", "spring-boot", "postgresql"]
categories = []
externalLink = ""
series = []
+++

![Image alt](transaction-routing.png)

**Prerequisite**: Primary and secondary replica of postgres should be setup before this demo can be run. I will be adding an article on how to setup read replica for a postgres db. 

**Motivation**: Redirect read only requests to read replica and read-write requests to primary db.

In this article we will see how we can configure read replicas for a primary db and based on the transaction type (READ_WRITE or READ ONLY) our spring boot application will select the correct datasource. This is a classic case when you want to minimize the load on your primary database by redirecting the read requests to replicas. 