+++ 
draft = false
date = 2023-09-29T09:45:36+05:30
title = "PostgreSQL Streaming Replication With Docker"
description = ""
slug = ""
authors = []
tags = ["database-read-replica","postgresql","docker"]
categories = []
externalLink = ""
series = []
+++


In this article , we will see how to configure replica of a postgres database. We will have two postgres instances with one being the primary and the other as secondary(replica). We will be running our databases in docker containers.  

First , lets start the primary database in a docker container using below command:

```sh
docker run -d \
  -p 5432:5432 \
  -e POSTGRES_PASSWORD=password \
  --name primary \
  -v /Users/pashmantak/workspace/docker-volume-mounts/primary:/var/lib/postgresql/data \
  postgres:14.5-alpine

```

With the above command , our primary database is running on port 5432 with user 'postgres' and password as 'password'. We have also added volume to map the postgres's data directory to host's directory.  

Lets connect to our primary database with below command:

```sh
docker exec -it primary bash
```

Create a directory in the primary db container at root level and lets call it 'backup' and take the backup of primary db for replication using below command:

```sh
pg_basebackup -D /backup/ -R -v -U postgres
```


Copy the backup file to the data directory of primary db. 

```sh
cp -r ./backup/ /var/lib/postgresql/data
```

Create a volume directory in the host for replica as well. 

``` sh
mkdir -p /Users/pashmantak/workspace/docker-volume-mounts/replica
```

Copy the backup from primary db's volume to replica container's (container is yet to be created) volume. 

```sh
cp -r /Users/pashmantak/workspace/docker-volume-mounts/primary/backup/ /Users/pashmantak/workspace/docker-volume-mounts/replica/
```


Move contents of the back folder we just copied in the previous step to the /Users/pashmantak/workspace/docker-volume-mounts/replica folder. 

```sh
mv /Users/pashmantak/workspace/docker-volume-mounts/replica/backup/* .
```

Now its time to change the postgres configuration files. Go to directory /Users/pashmantak/workspace/docker-volume-mounts/primary and open postgres.conf file.  Most of the values will be default and we will need to change for below parameters:

```sh
wal_log_hints = on
archive_mode = on
archive_command = 'test ! -f /var/lib/postgresql/data/archiver/%f && cp %p /var/lib/postgresql/data/archiver/%f'

wal_keep_size = 512

```

archive_mode is changed to 'on' to store the wal files. 


We will also modify hba.conf in the same directory as above, 