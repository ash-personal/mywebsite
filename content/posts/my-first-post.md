+++ 
draft = true
date = 2023-09-11T11:23:36+05:30
title = "Postgres primary and read replica demonstration using a spring boot project"
description = ""
slug = ""
authors = []
tags = ["database-read-replica", "spring-boot", "postgresql"]
categories = []
externalLink = ""
series = []
+++


![image](images/avatar.jpg)

In this article we will see how we can configure read replicas for a primary db and based on the transaction type (READ_WRITE or READ ONLY) our spring boot application will select the correct datasource. This is a classic case when you want to minimize the load on your primary database by redirecting the read requests to replicas. 