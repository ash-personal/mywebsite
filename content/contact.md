+++
title = "Contact"
slug = "contact"
+++

I would love to connect!!

Email: <ashmantak@gmail.com>\
Linkedin: [@ashmantak](https://www.linkedin.com/in/ashmantak-pandey-02211a12)\
Twitter: [@ashmantak12](https://twitter.com/ashmantak12)\
Gitlab: [@singularity_0](https://gitlab.com/singularity_0)\
Github: [ashmantak](https://github.com/ashmantak)